from datetime import datetime
import pandas as pd
import pytest
import pathlib

from earthquakes import usgs_api

latitude = 35.025
longitude = 25.763
radius = 200
minimum_magnitude = 4.5

CUR_DIR = pathlib.Path(__file__).parent
EXPECTED_EARTHQUAKE_DATA = pd.read_csv(CUR_DIR / "expected_earthquake_data.csv")


def test_build_api_url():
    latitude = 35.6895
    longitude = 139.6917
    maxradiuskm = 50.0
    starttime = datetime(2023, 9, 10, 10, 0, 0)
    endtime = datetime(2023, 9, 15, 10, 0, 0)
    format = "csv"

    url = usgs_api.build_api_url(latitude, longitude, maxradiuskm, starttime, endtime, format)

    assert f"latitude={latitude}" in url
    assert f"longitude={longitude}" in url
    assert f"maxradiuskm={maxradiuskm}" in url
    assert f"starttime={starttime.isoformat()}" in url
    assert f"endtime={endtime.isoformat()}" in url
    assert f"format={format}" in url


def test_latitude_type_error():
    with pytest.raises(TypeError) as exc_info:
        usgs_api.build_api_url(
            latitude="invalid_latitude",
            longitude=139,
            maxradiuskm=50,
            starttime=datetime(2023, 9, 10, 10, 0, 0),
            endtime=datetime(2023, 9, 15, 10, 0, 0),
            format="csv",
        )
    assert "latitude, longitude, maxradius must be of type float or int" in str(exc_info.value)


def test_longitude_type_error():
    with pytest.raises(TypeError) as exc_info:
        usgs_api.build_api_url(
            latitude=35.6895,
            longitude="invalid_longitude",
            maxradiuskm=50.0,
            starttime=datetime(2023, 9, 10, 10, 0, 0),
            endtime=datetime(2023, 9, 15, 10, 0, 0),
            format="csv",
        )
    assert "latitude, longitude, maxradius must be of type float or int" in str(exc_info.value)


def test_maxradiuskm_type_error():
    with pytest.raises(TypeError) as exc_info:
        usgs_api.build_api_url(
            latitude=35.6895,
            longitude=139.6917,
            maxradiuskm="invalid_maxradiuskm",
            starttime=datetime(2023, 9, 10, 10, 0, 0),
            endtime=datetime(2023, 9, 15, 10, 0, 0),
            format="csv",
        )
    assert "latitude, longitude, maxradius must be of type float or int" in str(exc_info.value)


def test_starttime_type_error():
    with pytest.raises(TypeError) as exc_info:
        usgs_api.build_api_url(
            latitude=35.6895,
            longitude=139,
            maxradiuskm=50,
            starttime="invalid_starttime",
            endtime=datetime(2023, 9, 15, 10, 0, 0),
            format="csv",
        )
    assert "starttime and endtime must be of type datetime" in str(exc_info.value)


def test_endtime_type_error():
    with pytest.raises(TypeError) as exc_info:
        usgs_api.build_api_url(
            latitude=35.6895,
            longitude=139.6917,
            maxradiuskm=50.0,
            starttime=datetime(2023, 9, 10, 10, 0, 0),
            endtime="invalid_endtime",
            format="csv",
        )
    assert "starttime and endtime must be of type datetime" in str(exc_info.value)


def test_invalid_maxradiuskm():
    with pytest.raises(ValueError) as exc_info:
        usgs_api.build_api_url(
            latitude=35.6895,
            longitude=139.6917,
            maxradiuskm=-10.0,
            starttime=datetime(2023, 9, 10, 10, 0, 0),
            endtime=datetime(2023, 9, 15, 10, 0, 0),
        )
    assert "maxradiuskm must be positive and less than" in str(exc_info.value)


def test_invalid_latitude():
    with pytest.raises(ValueError) as exc_info:
        usgs_api.build_api_url(
            latitude=-91.0,
            longitude=139.6917,
            maxradiuskm=50.0,
            starttime=datetime(2023, 9, 10, 10, 0, 0),
            endtime=datetime(2023, 9, 15, 10, 0, 0),
        )
    assert "latitude must be between" in str(exc_info.value)


def test_invalid_longitude():
    with pytest.raises(ValueError) as exc_info:
        usgs_api.build_api_url(
            latitude=35.6895,
            longitude=-181.0,
            maxradiuskm=50.0,
            starttime=datetime(2023, 9, 10, 10, 0, 0),
            endtime=datetime(2023, 9, 15, 10, 0, 0),
        )
    assert "longitude must be between" in str(exc_info.value)


def test_starttime_greater_than_endtime():
    with pytest.raises(ValueError) as exc_info:
        usgs_api.build_api_url(
            latitude=35.6895,
            longitude=139.6917,
            maxradiuskm=50.0,
            starttime=datetime(2023, 9, 15, 10, 0, 0),
            endtime=datetime(2023, 9, 10, 10, 0, 0),
        )
    assert "starttime must be inferior to endtime" in str(exc_info.value)


def test_invalid_format():
    with pytest.raises(ValueError) as exc_info:
        usgs_api.build_api_url(
            latitude=35.6895,
            longitude=139.6917,
            maxradiuskm=50.0,
            starttime=datetime(2023, 9, 10, 10, 0, 0),
            endtime=datetime(2023, 9, 15, 10, 0, 0),
            format="invalid_format",
        )
    assert "format must be in" in str(exc_info.value)


@pytest.fixture
def earthquake_data():
    # NOTE: This request may take significant time (>10s)
    return usgs_api.get_earthquake_data(
        latitude=latitude,
        longitude=longitude,
        radius=radius,
        minimum_magnitude=minimum_magnitude,
        start_date=datetime(year=1821, month=10, day=21, second=34),
        end_date=datetime(year=2021, month=10, day=21, second=34),
    )


def test_earthquake_data_type(earthquake_data):
    assert isinstance(earthquake_data, pd.DataFrame)


def test_earthquake_data_length(earthquake_data):
    assert len(earthquake_data) == 734


def test_earthquake_data_content(earthquake_data):
    earthquake_data_sample = earthquake_data[
        earthquake_data["time"].isin(EXPECTED_EARTHQUAKE_DATA["time"])
    ]

    pd.testing.assert_frame_equal(
        earthquake_data_sample.drop("updated", axis=1),
        EXPECTED_EARTHQUAKE_DATA.drop("updated", axis=1),
    )
