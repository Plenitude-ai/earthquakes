import pandas as pd

from earthquakes import tools

LATITUDE = 35.025
LONGITUDE = 25.763


def test_haversine_distance():
    distance = tools._haversine_distance(34.9850, 25.8020, LATITUDE, LONGITUDE)
    assert 5.69 < distance < 5.70


def test_get_haversine_distance():
    latitudes = pd.Series([34.9371, 34.9610])
    longitudes = pd.Series([25.7577, 25.6500])
    distances = tools.get_haversine_distance(latitudes, longitudes, LATITUDE, LONGITUDE)
    assert 9.79 < distances[0] < 9.80
    assert 12.52 < distances[1] < 12.53
