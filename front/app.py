from datetime import datetime, timedelta
import pathlib

import streamlit as st

import earthquakes.usgs_api

CUR_DIR = pathlib.Path(__file__).parent
st.set_page_config(page_title="Earthquakes", page_icon=str(CUR_DIR / "src/earthquake.jpg"))

st.title("Earthquakes")

st.text("Find recent earthquakes in your region")
st.markdown("---")

st.text("Your location :")
with st.container():
    col1, col2 = st.columns([1, 1])
    latitude = col1.number_input(label="latitude", min_value=-90.0, max_value=+90.0, value=48.8608)
    longitude = col2.number_input(
        label="longitude", min_value=-90.0, max_value=+90.0, value=2.3373
    )
st.markdown("---")
st.text("Research parameters")
with st.container():
    col1, col2 = st.columns([1, 1])
    radius = col1.slider(label="Distance (km)", min_value=0, max_value=630, value=400)
    minimum_magnitude = col2.slider(
        label="Magnitude minimum :", min_value=0, max_value=10, value=4
    )
    delta_years = col1.slider(label="Years span", min_value=10, max_value=200, value=100)


send = st.button(label="Search")
st.markdown("---")
if send:
    start_date = datetime.today() - timedelta(days=int(365.25 * delta_years))
    data = earthquakes.usgs_api.get_earthquake_data(
        latitude=latitude,
        longitude=longitude,
        radius=radius,
        minimum_magnitude=minimum_magnitude,
        start_date=start_date,
    )
    st.text(f"Number of earthquakes : {len(data)}")
    st.map(data=data)
