import math

import pandas as pd

EARTH_RADIUS = 6_378
API_MAX_RADIUS_KM = 20_001.6
API_MAX_LONGITUDE = 180
API_MIN_LONGITUDE = -180
API_MAX_LATITUDE = 90
API_MIN_LATITUDE = -90

TIME_COLUMN = "time"
PAYOUT_COLUMN = "payout"
MAGNITUDE_COLUMN = "mag"
DISTANCE_COLUMN = "distance"
LATITUDE_COLUMN = "latitude"
LONGITUDE_COLUMN = "longitude"


def _haversine_distance(lat_x: float, lon_x: float, lat_y: float, lon_y: float):
    """
    Computes Haversine distance between x and y
    """
    # Convert latitude and longitude from degrees to radians
    lat_x, lon_x, lat_y, lon_y = map(math.radians, [lat_x, lon_x, lat_y, lon_y])

    first_member = math.pow(math.sin((lat_y - lat_x) / 2), 2)
    second_member = math.cos(lat_x) * math.cos(lat_y) * math.pow(math.sin((lon_y - lon_x) / 2), 2)
    h = 2 * EARTH_RADIUS * math.asin(math.sqrt(first_member + second_member))
    return h


def get_haversine_distance(
    earthquake_latitudes: pd.Series,
    earthquake_longitudes: pd.Series,
    reference_latitude: float,
    reference_longitude: float,
) -> pd.Series:
    """
    Computes the distance between two points on a sphere

    Returns the distance between all the given earthquake's epicenters,
    and the reference point

    **ATTENTION** : Here I considered that EARTH_RADIUS is way bigger than the
    depth of the actual epicenter, meaning we can approximate all the
    epicenters of the earthquakes to be at the surface, and consequently on
    the same depth
    If this assumption does not hold, we should implement a different distance,
    that takes the depth additionnaly to the latitude, longitue coordinates
    """
    all_points = pd.DataFrame(
        list(zip(earthquake_latitudes, earthquake_longitudes)),
        columns=["earthquake_latitude", "earthquake_longitude"],
    )

    distances = all_points.apply(
        lambda row: _haversine_distance(
            lat_x=row.earthquake_latitude,
            lon_x=row.earthquake_longitude,
            lat_y=reference_latitude,
            lon_y=reference_longitude,
        ),
        axis=1,
    )
    return distances
