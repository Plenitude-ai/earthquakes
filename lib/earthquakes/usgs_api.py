import asyncio
import aiohttp
import urllib.request
from typing import Union

import io
from datetime import datetime

import pandas as pd

from . import tools


def build_api_url(
    latitude: float,
    longitude: float,
    maxradiuskm: float,
    starttime: datetime,
    endtime: datetime,
    format: str = "csv",  # noqa
) -> str:
    """TODO to test"""
    if (
        not isinstance(latitude, Union[float, int])
        or not isinstance(longitude, Union[float, int])
        or not isinstance(maxradiuskm, Union[float, int])
    ):
        raise TypeError("latitude, longitude, maxradius must be of type float or int")
    if not isinstance(starttime, datetime) or not isinstance(endtime, datetime):
        raise TypeError("starttime and endtime must be of type datetime")

    correct_formats = ["csv", "xml", "kml", "geojson", "quakeml", "text"]
    if format not in correct_formats:
        raise ValueError(f"format must be in {correct_formats} : given {type(format)}")

    if (maxradiuskm < 0) or (maxradiuskm > tools.API_MAX_RADIUS_KM):
        raise ValueError(f"maxradiuskm must be positive and less than {tools.API_MAX_RADIUS_KM}")
    if (latitude < tools.API_MIN_LATITUDE) or (latitude > tools.API_MAX_LATITUDE):
        raise ValueError(
            f"latitude must be between {tools.API_MIN_LATITUDE} and {tools.API_MAX_LATITUDE}"
        )
    if (longitude < tools.API_MIN_LONGITUDE) or (longitude > tools.API_MAX_LONGITUDE):
        raise ValueError(
            f"longitude must be between {tools.API_MIN_LONGITUDE} and {tools.API_MAX_LONGITUDE}"
        )
    if starttime > endtime:
        raise ValueError("starttime must be inferior to endtime")
    url = (
        f"https://earthquake.usgs.gov/fdsnws/event/1/query?"
        f"format={format}&latitude={latitude}&longitude={longitude}&maxradiuskm={maxradiuskm}"
        f"&starttime={starttime.isoformat()}&endtime={endtime.isoformat()}"
    )
    return url


def get_earthquake_data(
    latitude: float = 48.866667,
    longitude: float = 2.333333,
    radius: float = 100,
    minimum_magnitude: float = 4.0,
    start_date: datetime = None,
    end_date: datetime = datetime.today(),
) -> pd.DataFrame:
    """TODO

    + The function will retrieve the earthquake data of the area of interest for the past 200 years,
    + The implementation must use the `urllib` python package,
    + The API request url must be build in a dedicated function `build_api_url`,
    + Tests should be provided for `build_api_url`.

    Note: Earthquakes after the 21-10-2021 should not be considered.
    """
    YEAR_SPAN_DEFAULT = 50
    if start_date is None:
        start_date = datetime(
            end_date.year - YEAR_SPAN_DEFAULT, month=end_date.month, day=end_date.day
        )

    url = build_api_url(
        latitude=latitude,
        longitude=longitude,
        maxradiuskm=radius,
        starttime=start_date,
        endtime=end_date,
    )
    req = urllib.request.Request(url=url)
    with urllib.request.urlopen(req) as response:
        data = pd.read_csv(io.BytesIO(response.read()))
    data = data[(data.mag >= minimum_magnitude) & (data.type == "earthquake")].reset_index(
        drop=True
    )
    return data


async def _get_earthquake_data_coroutine(
    session: aiohttp.ClientSession,
    latitude: float,
    longitude: float,
    radius: float,
    minimum_magnitude: float,
    end_date: datetime = datetime(year=2021, month=10, day=21),
) -> pd.DataFrame:
    starttime = datetime(end_date.year - 200, month=end_date.month, day=end_date.day)

    url = build_api_url(
        latitude=latitude,
        longitude=longitude,
        maxradiuskm=radius,
        starttime=starttime,
        endtime=end_date,
    )
    async with session.get(url) as response:
        data = pd.read_csv(io.BytesIO(await response.read()))
    data = data[(data.mag >= minimum_magnitude) & (data.type == "earthquake")].reset_index(
        drop=True
    )
    return data


async def get_earthquake_data_for_multiple_locations(
    assets: list[tuple[float, float]],
    radius: float = 200,
    minimum_magnitude: float = 4.5,
    end_date: datetime = datetime(year=2021, month=10, day=21),
) -> pd.DataFrame:
    """Request the USGS api for multiple assets, in async mode

    Relies on the asyncio and aiohttp packages"""
    async with aiohttp.ClientSession() as session:
        tasks = []
        for latitude, longitude in assets:
            task = asyncio.create_task(
                _get_earthquake_data_coroutine(
                    session,
                    latitude=latitude,
                    longitude=longitude,
                    radius=radius,
                    minimum_magnitude=minimum_magnitude,
                    end_date=end_date,
                )
            )
            tasks.append(task)
        results = await asyncio.gather(*tasks)
    earthquakes_data = pd.concat(results)
    earthquakes_data = (
        earthquakes_data.sort_values(by="updated")
        .drop_duplicates(keep="last")
        .reset_index(drop=True)
    )
    return earthquakes_data
