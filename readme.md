# Earthquakes

<img src="https://cdn.britannica.com/69/249569-050-D53B19D1/map-Morocco-earthquake-2023.jpg" alt="alt" title="Morocco's 2023 earthquake" width="40%">


Following Morocco's earthquake disaster, I wanted to investigate the potential risk of having an earhtquake in different areas. I created a small app to view the last earthquake events in my neighboorhood, using USGS's [api](https://earthquake.usgs.gov/fdsnws/event/1/) with all earthquakes detected in the last 200+ years.

My app is deployed on an ECS cluster, with a Load balancer to attach an Elastic IP.
It is visible at [earthquakes.plenitude-ai.fr](http://earthquakes.plenitude-ai.fr)

Thanks for checking out my work !


<img src="./data/screenshot_earthquake_app.png" alt="alt" title="Screenshot of the app running on earthquakes.plenitude-ai.fr" width="60%">